﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoyerMoore
{
    class Program
    {
        public static char[] Symbols; // Таблица символов
        public static int[] Shifts; // Таблица сдвигов

        public static void Main()
        {
            string template = "abc";
            string str =      "sabccdadfaascabccdasdde";
            bool has, have;
            int l, j, i;
            ShiftTable(template);
            List<int> nums = new List<int>(); // Список вхождений шаблона
            if (template.Length <= str.Length) 
            {
                for (i = 0; i < str.Length - template.Length + 1; i++)
                {
                    j = template.Length - 1;
                    have = true;
                    while ((j >= 0) && (have))
                    {
                        if (str[i + j] != template[j])
                        {
                            have = false;
                            if (j == template.Length - 1)
                            {
                                l = 0;
                                has = false;
                                while ((l < template.Length) && (!has))
                                {
                                    if (str[i + j] == Symbols[l])
                                    {
                                        has = true;
                                        i = i + Shifts[l] - 1;
                                    }
                                    l++;
                                }
                                if (!has)
                                {
                                    i = i + template.Length - 1;
                                }
                            }
                        }
                        j--;
                    }
                    if (have)
                        nums.Add(i);
                }
                foreach (var num in nums)
                    Console.Write("{0} ", num);
            }
            else 
                Console.WriteLine("Template is longer then string");
            Console.ReadKey();
        }

        // Создание таблицы сдвигов
        public static void ShiftTable(string template)
        {
            int j;
            int k = 0;
            bool fl;
            Symbols = new char[template.Length];
            Shifts = new int[template.Length];

            for (int i = template.Length - 2; i >= 0; i--)
            {
                fl = false;
                j = 0;
                while ((j < k + 1) && (!fl))
                {
                    if (Symbols[j] == template[i])
                    {
                        fl = true;
                    }
                    j++;
                }
                if (!fl)
                {
                    Symbols[k] = template[i];
                    Shifts[k] = template.Length - i - 1;
                    k++;
                }
            }
        }
    }
}
