﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamming
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> message = new List<int> {1,0,1,1,0,1,0,0,1};
            Print(message);
            for (var i = 0; Math.Pow(2, i) < message.Count; i++)
            {
                var pos = Convert.ToInt32(Math.Pow(2, i)) - 1;
                message.Insert(pos, 0);
            }
            ControlBits(message);
            Print(message);
            message[11] = 1;
            Print(message);
            ControlBits(message);
            Print(message);
            string error = "";
            for (var i = 0; Math.Pow(2, i) < message.Count; i++)
            {
                var pos = Convert.ToInt32(Math.Pow(2, i)) - 1;
                error += message[pos];
            }
            char[] arr = error.ToCharArray();
            Array.Reverse(arr);
            var result = Convert.ToInt32(new string(arr), 2);
            if (result != 0)
            {
                Console.WriteLine(result - 1);
            }
            Console.ReadKey();
        }

        public static void Print(List<int> message)
        {
            foreach (var bit in message)
            {
                Console.Write(bit);
            }
            Console.WriteLine();
        }

        public static void ControlBits(List<int> message)
        {
            for (var i = 0; Math.Pow(2, i) < message.Count; i++)
            {
                int count = 0;
                var pos = Convert.ToInt32(Math.Pow(2, i)) - 1;
                for (var j = pos; j < message.Count; j += pos+1)
                {
                    var inc = 0;
                    for (var k = 0; k < pos+1 && j+k < message.Count; k++)
                    {
                        count += message[j + k];
                        inc++;
                    }
                    j += inc;
                }
                message[pos] = count % 2;
            }
        }
    }
}
