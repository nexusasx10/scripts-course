﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using System.IO;

namespace LifeGame
{
    class Program
    {
        static void Main(string[] args)
        {
            bool go = false;
            Game.Build();
            Game.Neighbors();
            bool set = false;
            while(true)
            {
                if (go)
                {
                    Game.Update();
                    Game.Draw();
                    Thread.Sleep(80);
                }
                else
                {
                    if (!set)
                    {
                        Game.Update();
                        Game.Draw();
                    }
                    string change = Console.ReadLine();
                    if (change != "")
                    {
                        if (change == "go")
                        {
                            go = true;
                            continue;
                        }
                        set = true;
                        string[] newElem = change.Split(',');
                        var m = Convert.ToInt32(newElem[0]);
                        var l = Convert.ToInt32(newElem[1]);
                        Game.nextGeneration[m, l] = !Game.currentGeneration[m, l];
                        Game.Draw();
                    }
                    else
                        set = false;
                }
            }
        }
    }

    public class Game
    {
        public const int size = 11;
        public static bool[,] currentGeneration = new bool[size, size];
        public static bool[,] nextGeneration = new bool[size, size];
        public static string[,] neighbors = new string[size, size];

        public static void Neighbors()
        {
            for (var x = 0; x < size; x++)
            {
                for (var y = 0; y < size; y++)
                {
                    for (int i = -1; i < 2; i++)
                    {
                        for (int j = -1; j < 2; j++)
                        {
                            if ((i == 0 && j != 0) || (i != 0 && j == 0) || (i != 0 && j != 0))
                            {
                                if (x + i < 0)
                                    neighbors[x, y] += size - 1;
                                else if (x + i > size -1)
                                    neighbors[x, y] += 0;
                                else
                                    neighbors[x, y] += x + i;

                                if (y + j < 0)
                                    neighbors[x, y] += size - 1;
                                else if (y + j > size -1)
                                    neighbors[x, y] += 0;
                                else
                                    neighbors[x, y] += y + j;
                            }
                        }
                    }
                }
            }
        }

        public static void Build()
        {
            nextGeneration[1, 2] = true;
            nextGeneration[2, 2] = true;
            nextGeneration[3, 2] = true;
            nextGeneration[3, 1] = true;
            nextGeneration[2, 0] = true;
        }

        public static void Update()
        {
            for (var x = 0; x < size; x++)
            {
                for (var y = 0; y < size; y++)
                {
                    int count = 0;
                    for (var i = 0; i < 8; i++)
                    {
                        var l = Convert.ToInt32(Convert.ToString(neighbors[x, y][2*i]));
                        var m = Convert.ToInt32(Convert.ToString(neighbors[x, y][2*i + 1]));

                        if (currentGeneration[l, m])
                            count++;
                    }
                    if (!currentGeneration[x, y] && (count == 3 ))
                    {
                        nextGeneration[x, y] = true;
                    }
                    if (currentGeneration[x, y] && (count < 2 || count > 3))
                    {
                        nextGeneration[x, y] = false;
                    }
                }
            }
        }

        public static void Draw()
        {
            Console.Clear();
            for (var x = 0; x < size; x++)
            {
                for (var y = 0; y < size; y++)
                {
                    currentGeneration[x, y] = nextGeneration[x, y];
                    if (nextGeneration[x, y])
                    {
                        Console.Write('0');
                    }

                    else
                    {
                        Console.Write('_');
                    }
                }
                Console.Write('\n');
            }
        }
    }
}
