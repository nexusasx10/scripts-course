﻿using System;
using System.IO;
using System.Collections.Generic;
using AdditionalMethodsLibrary;

namespace CesarCipher
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "../../../ ", "files");
            string message = "обычныйтексткакойто";
            var alphabet = Linguistics.Alphabet("rus");

            Console.WriteLine(message);
            string code = Shift(message, 5, alphabet);
            Console.WriteLine(code);
            int shift = Break(code, alphabet);
            Console.WriteLine(shift);
            string decode = Shift(code, shift, alphabet);
            Console.WriteLine(decode);
            Console.ReadKey();
        }

        public static string Shift(string message, int shift, List<char> alphabet)
        {
            string codedMessage = "";
            for (var num = 0; num < message.Length; num++)
            {
                if (alphabet.IndexOf(message[num]) + shift > alphabet.Count)
                    codedMessage += Convert.ToChar(Convert.ToInt32(message[num]) + (shift - 32));
                else
                    codedMessage += Convert.ToChar(Convert.ToInt32(message[num]) + shift);
            }
            return codedMessage;
        }

        public static int Break(string message, List<char> alphabet)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "../../../ ", "files");
            string[,] table = Table.GetTableFromFile(File.ReadAllLines(Path.Combine(path, "GlobalFrequencyTable.txt")), ' ');

            SortedDictionary<char, double> globalFrequencyTable = new SortedDictionary<char, double>();
            for (var i = 0; i < table.GetLength(0); i++)
            {
                var character = Convert.ToChar(table[i, 0]);
                var frequency = Convert.ToDouble(table[i, 1]);
                globalFrequencyTable.Add(character, frequency);
            }
            Dictionary<char, int> countedAlphabet = Linguistics.GetCountedAlphabet(message);
            SortedDictionary<char, double> localFrequencyTable = Linguistics.GetSortedFrequencyTable(countedAlphabet, message);

            int shift = 0;
            double sum = int.MaxValue;
            for (var s = 0; s < globalFrequencyTable.Count; s++)
            {
                char shiftedCharacter = ' ';
                double newSum = 0;
                foreach (var character in alphabet)
                {
                    shiftedCharacter = Convert.ToChar(Convert.ToInt32(character) + s);
                    if (alphabet.IndexOf(shiftedCharacter) == -1)                        
                        shiftedCharacter = Convert.ToChar(Convert.ToInt32(shiftedCharacter) - alphabet.Count);
                    newSum += Math.Pow(globalFrequencyTable[shiftedCharacter] - localFrequencyTable[character], 2);
                }
                if (newSum < sum)
                {
                    sum = newSum;
                    shift = s;
                }
            }
            return shift;
        }
    }
}
