﻿using System;
using System.IO;

namespace RLE
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "../../../ ", "files");

            Console.WriteLine("Введите новое значение input или оставте пустым, чтобы использовать предыдущее значение:");
            var textFromConsole = Console.ReadLine();
            if (textFromConsole != "")
                File.WriteAllText(Path.Combine(path, "input.txt"), textFromConsole);

            string input = File.ReadAllText(Path.Combine(path, "input.txt"));

            Escape.Encode(input);
            string ecode = File.ReadAllText(Path.Combine(path, "escape.code.txt"));
            Escape.Decode(ecode);
            
            Jump.Encode(input);
            string jcode = File.ReadAllText(Path.Combine(path, "jump.code.txt"));
            Jump.Decode(jcode);            
            
            string eoutput = File.ReadAllText(Path.Combine(path, "escape.output.txt"));
            string joutput = File.ReadAllText(Path.Combine(path, "jump.output.txt"));

            if (input != "")
            {
                Console.WriteLine("Escape:");
                if (input == eoutput)
                {
                    Console.WriteLine("Status: success");
                    Console.WriteLine("Compression ratio: " + Math.Round(((1 - (double)ecode.Length / (double)input.Length) * 100), 2) + "%");
                }
                else
                {
                    Console.WriteLine("Status: not correct");
                }

                Console.WriteLine("");

                Console.WriteLine("Jump:");
                if (input == joutput)
                {
                    Console.WriteLine("Status: success");
                    Console.WriteLine("Compression ratio: " + Math.Round(((1 - (double)jcode.Length / (double)input.Length) * 100), 2) + "%");
                }
                else
                {
                    Console.WriteLine("Status: Not correct");
                }
            }
            else
            {
                Console.WriteLine("Error: input is empty");
            }
            Console.ReadKey();
        }
    }
}