﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLE
{
    class AdditionalMethods
    {
        public static char AddFlag(int mode, int count)
        {
            char symbol = Convert.ToChar(count);
            string symbolBin = Convert.ToString(symbol, 2);
            while (symbolBin.Length < 7)
                symbolBin = "0" + symbolBin;
            symbolBin = mode + symbolBin;
            return Convert.ToChar(Convert.ToInt32(symbolBin, 2));
        }
    }
}
