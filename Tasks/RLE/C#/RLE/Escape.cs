﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLE
{
    class Escape
    {
        public static void Encode(string input)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "../../../ ", "files");
            string code="";
            var count = 1;
            for (var num = 0; num < input.Length; num++)
            {
                if (num != input.Length-1 && input[num] == input[num + 1] && count < 256)
                {
                    count++;
                }
                else
                {
                    if (count > 1 || input[num] == '#')
                    {
                        if (count > 3 || input[num] == '#')
                        {
                            code += '#';
                            code += Convert.ToChar(count);
                            count = 1;
                        }
                        else
                        {
                            for (var i = 0; i < count-1; i++)
                            {
                                code += input[num];
                            }
                            count = 1;
                        }
                    }
                    code += input[num];
                }
            }
            File.WriteAllText(Path.Combine(path, "escape.code.txt"), code);
        }

        public static void Decode(string code)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "../../../ ", "files");
            string output = "";
            for (var num = 0; num < code.Length; num++)
            {
                if (code[num] == '#')
                {
                    var count = code[num + 1];
                    for (var i = 0; i < count; i++)
                    {
                        output += code[num + 2];
                    }
                    num += 2;
                }
                else
                {
                    output += code[num];
                }
            }
            File.WriteAllText(Path.Combine(path, "escape.output.txt"), output);
        }
    }

}
