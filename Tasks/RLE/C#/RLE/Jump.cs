﻿using System;
using System.IO;

namespace RLE
{
    class Jump
    {
        public static void Encode(string input)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "../../../ ", "files");

            string code = "";
            var count = 1;
            string sequence = "";

            for (var num = 0; num < input.Length; num++)
            {
                if ((num != input.Length - 1 && input[num] == input[num + 1]) && count < 127)
                {
                    if (sequence.Length > 0)
                    {
                        code += AdditionalMethods.AddFlag(0, sequence.Length);
                        code += sequence;
                        sequence = "";
                    }
                    count++;
                }
                if ((num == input.Length - 1 || input[num] != input[num + 1]) && sequence.Length < 127)
                {
                    if (count > 2)
                    {
                        code += AdditionalMethods.AddFlag(1, count);
                        code += input[num];
                        count = 1;
                        continue;
                    }
                    if (count == 2)
                    {
                        sequence += input[num - 1];
                        sequence += input[num];
                        count = 1;
                        continue;
                    }
                    sequence += input[num];
                }
                if (count == 127)
                {
                    code += AdditionalMethods.AddFlag(1, count - 1);
                    code += input[num];
                    count = 1;
                }
                if (sequence.Length == 127)
                {
                    code += AdditionalMethods.AddFlag(0, sequence.Length);
                    code += sequence;
                    sequence = "";
                }
            }
            if (sequence.Length > 0)
            {
                code += AdditionalMethods.AddFlag(0, sequence.Length);
                code += sequence;
                sequence = "";
            }

            File.WriteAllText(Path.Combine(path, "jump.code.txt"), code);
        }

        public static void Decode(string code)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "../../../ ", "files");

            string output = "";

            for (var num = 0; num < code.Length; num++)
            {
                var symbol = code[num];
                string symbolBin = Convert.ToString(symbol, 2);
                while (symbolBin.Length < 8)
                    symbolBin = "0" + symbolBin;
                var mode = Convert.ToInt32(Convert.ToString(symbolBin[0]));
                symbolBin = symbolBin.Substring(1,symbolBin.Length-1);
                var count = Convert.ToInt64(symbolBin, 2);
                num++;
                if (mode == 0)
                    for (var i = 0; i < count; i++)
                    {
                        output += code[num];
                        if (i != count - 1)
                            num++;
                    }
                else
                {
                    for (var i = 0; i < count; i++)
                    {
                        output += code[num];
                    }
                }
            }
            File.WriteAllText(Path.Combine(path, "jump.output.txt"), output);
        }
    }
    
}
