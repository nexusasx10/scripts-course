﻿using System;
using System.Collections.Generic;
using System.IO;
using AdditionalMethodsLibrary;

namespace Entropy
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "../../../ ", "files");
            string message = "abb";//File.ReadAllText(Path.Combine(path, "input.txt"));

            Dictionary<char, int> countedAlphabet = Alpha.GetCountedAlphabet(message);            
            Dictionary<char, double> frequencyTable = Alpha.GetFrequencyTable(countedAlphabet, message);
            double entropy = 0;

            foreach (var character in frequencyTable)
            {
                Console.WriteLine("{0} - {1}", character.Key, character.Value);
                entropy -= character.Value * Math.Log(character.Value) / Math.Log(2);
            }
            Console.WriteLine();
            Console.WriteLine("Entropy: {0}", entropy);
            Console.ReadKey();
        }
    }
}