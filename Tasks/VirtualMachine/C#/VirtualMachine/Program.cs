﻿using System;
using System.IO;

namespace VirtualMachine
{
    class Program
    {
        // Создание и инициализация вирутальной памяти
        public static string[] memory = new string[128];
        public static int ip = 0;

        // Загрузка программы
        public static void Load()
        {
            int commandNum = 0;
            string filename = Console.ReadLine();
            string path = Path.Combine(Environment.CurrentDirectory, "../../", "Programs\\", filename + ".txt");
            if (!File.Exists(path))
            {
                Console.WriteLine("Программа не найдена!");
                memory[commandNum] = "exit";
                return;
            }
            string[] file = File.ReadAllLines(path);
            string program = "";
            foreach (var line in file)
            {
                program += line + " ";
            }
            var commands = program.Split(' ');
            for (commandNum = 0; commandNum < commands.Length - 1; commandNum++)
            {
                memory[commandNum] = commands[commandNum];
            }
            if (memory[commandNum] != "exit")
                memory[commandNum] = "exit";
        }

        // Дамп памяти
        public static void Dump()
        {
            string pointer;
            var i = 0;
            foreach (var e in memory)
            {
                pointer = "";
                if (i == ip)
                    pointer = ">";
                var template = "{0,-2}{1:D2}: {2,-20}";
                if (e != null)
                    Console.WriteLine(template, pointer, i, e);
                else
                    Console.WriteLine(template, pointer, i, "<null>");
                i++;
            }
        }

        // Обращение к элементу памяти
        public static int Memory(int ip)
        {
            return int.Parse(memory[ip]);
        }

        // Точка входа
        static void Main()
        {
            Load();
            while ((memory[ip] != "exit") && (ip != memory.Length-1))
                switch (memory[ip])
                {
                    // ВВОД-ВЫВОД //-------------------------------------------------------------------------------------------------------------

                    // Присваивание значения вводом из консоли
                    case "readcon":
                        Console.WriteLine("Введите число:");
                        memory[Memory(ip + 1)] = Console.ReadLine();
                        ip += 2;
                        break;

                    // Присваивание значения из другой переменной
                    case "readvar":
                        memory[Memory(ip + 2)] = memory[Memory(ip + 1)];
                        ip += 3;
                        break;

                    // Присваивание значения из числа
                    case "readnum":
                        memory[Memory(ip + 2)] = memory[ip + 1];
                        ip += 3;
                        break;

                    // Вывод значения
                    case "write":
                        Console.WriteLine(memory[Memory(ip + 1)]);
                        ip += 2;
                        break;

                    // ОПЕРАЦИИ //---------------------------------------------------------------------------------------------------------------

                    // Сумма
                    case "summ":
                        memory[Memory(ip + 3)] = (Memory(Memory(ip + 1)) + Memory(Memory(ip + 2))).ToString();
                        ip += 4;
                        break;

                    // Разность
                    case "subt":
                        memory[Memory(ip + 3)] = (Memory(Memory(ip + 1)) - Memory(Memory(ip + 2))).ToString();
                        ip += 4;
                        break;

                    // Умножение
                    case "mult":
                        memory[Memory(ip + 3)] = (Memory(Memory(ip + 1)) * Memory(Memory(ip + 2))).ToString();
                        ip += 4;
                        break;

                    // Деление нацело
                    case "divi":
                        memory[Memory(ip + 3)] = (Memory(Memory(ip + 1)) / Memory(Memory(ip + 2))).ToString();
                        ip += 4;
                        break;

                    // Остаток от деления
                    case "divm":
                        memory[Memory(ip + 3)] = (Memory(Memory(ip + 1)) % Memory(Memory(ip + 2))).ToString();
                        ip += 4;
                        break;

                    // ПЕРЕМЕЩЕНИЕ УКАЗАТЕЛЯ //--------------------------------------------------------------------------------------------------

                    // Сместить указатель вправо на значение
                    case "goto":
                        ip += Memory(ip + 1);
                        break;

                    // Переместить указатель памяти если условие верно
                    case "gotoife":
                        if (Memory(Memory(ip + 1)) == Memory(Memory(ip + 2)))
                            ip = Memory(ip + 3);
                        else
                            ip += 4;
                        break;

                    // Переместить указатель памяти если условие не верно
                    case "gotoifn":
                        if (Memory(Memory(ip + 1)) != Memory(Memory(ip + 2)))
                            ip = Memory(ip + 3);
                        else
                            ip += 4;
                        break;

                    // ТЕХНИЧЕСКИЕ //------------------------------------------------------------------------------------------------------------

                    // Пауза
                    case "pause":
                        Console.ReadKey();
                        ip += 1;
                        break;

                    // Дамп
                    case "dump":
                        Dump();
                        ip += 1;
                        break;

                    // Неопознанная команда
                    default:
                        Console.WriteLine("ERROR: Unidentified command. CODE: " + ip + ".");
                        ip = memory.Length-1;
                        break;
                }
            Console.ReadKey();
        }
    }
}