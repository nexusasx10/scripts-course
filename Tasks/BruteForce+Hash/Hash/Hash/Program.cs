﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hash
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = "abaaabcabbasab";
            string template = "ab";
            List<int> nums = new List<int>();
            int positions = input.Length - template.Length + 1;
            int tempSumm = 0;
            int inpSumm = 0;
            var col = 0;
            for (var i = 0; i < template.Length; i++)
	        {
                tempSumm += template[i];
                inpSumm += input[i];
            }

            for (var i = 0; i < positions; i++)
            {
                if (i != 0)
                {
                    inpSumm = inpSumm - input[i - 1] + input[i + template.Length - 1];
                }
                
                if (inpSumm == tempSumm)
                {
                    int j = 0;
                    while (j < template.Length && input[i + j] == template[j])
                    {
                        j++;
                    }
                    if (j == template.Length)
                    {
                        nums.Add(i);
                    }
                    else
                    {
                        col++;
                    }
                }
            }
            foreach (var num in nums)
            {
                Console.Write("{0} ", num);
            }
            Console.WriteLine();
            Console.WriteLine(col);
            Console.ReadKey();
        }
    }
}
