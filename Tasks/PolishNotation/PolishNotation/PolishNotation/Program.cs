﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PolishNotation
{
    class Program
    {
        public static List<string> input = new List<string> { "2", "/", "3", "+", "4", "-", "5", "*", "7", "-", "4" };
        public static List<string> output = new List<string>();
        public static List<string> stack = new List<string>();
        public static Dictionary<string, int> prior = new Dictionary<string, int>();
        static void Main(string[] args)
        {
            foreach (var op in input)
            {
                Console.Write(op);
            }
            Console.WriteLine();
            prior["("] = 0;
            prior[")"] = 1;
            prior["+"] = 2;
            prior["-"] = 2;
            prior["*"] = 3;
            prior["/"] = 3;
            int res;
            int counter = 0;
            foreach (var op in input)
            {
                if (Int32.TryParse(op, out res))
                {
                    output.Add(op);
                }
                else
                {
                    AddNew(op);
                }
                if (counter == input.Count - 1)
                {
                    for (var num = stack.Count - 1; num >= 0; num--)
                    {
                        output.Add(stack[num]);
                    }
                }
                counter++;
            }
            foreach (var op in output)
            {
                Console.Write(op);
            }
            Console.ReadKey();
        }

        public static void AddNew(string op)
        {
            if (stack.Count == 0 || prior[op] == 0)
            {
                stack.Add(op);
            }
            else
            {
                for (var num = stack.Count-1; num >= 0; num--)
                {
                    if (prior[stack[num]] >= prior[op])
                    {
                        output.Add(stack[num]);
                        stack.RemoveAt(stack.Count - 1);
                    }
                    else
                    {
                        break;
                    }
                }
                if (prior[op]==1)
                {
                    stack.RemoveAt(stack.Count - 1);
                }
                else
                {
                    stack.Add(op);
                }
            }
        }
    }
}
