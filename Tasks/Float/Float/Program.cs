﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Float
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberType type;
            string[] input = new string[] 
            {
                "0.5"
            };
            int maxLength = 0;
            foreach (var number in input)
            {
                if (maxLength < number.Length)
                {
                    maxLength = number.Length;
                }
            }
            string format = "{0,-" + maxLength + "} - {1}";
            foreach (var number in input)
            {
                string output = Translate(number, out type);
                if (type == NumberType.Number)
                {
                    Console.WriteLine(format, number, output);
                }
                else
                {
                    Console.WriteLine(format, type, output);
                }
            }

            string expression = "17.5+3.2+25.4";
            string result = Calculate(expression);
            string[] numbers = expression.Split('+');
            double decResult = 0;
            foreach (var number in numbers)
            {
                Console.WriteLine("{0,-4} - {1}", number, Translate(number, out type));
                decResult += Convert.ToDouble(number, CultureInfo.InvariantCulture);
            }
            Console.WriteLine("{0} - {1}", decResult, result);
            Console.ReadKey();
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////

        public enum NumberType
        {
            Number,
            NaN,
            Infinity
        }

        public static string Translate(string input, out NumberType type)
        {
            // Отсеиваем NaN
            if (input == "")
            {
                type = NumberType.NaN;
                return "1 11111111 10000000000000000000000";
            }

            string sign;
            if (input[0] == '-')
            {
                sign = "1";
                input = input.Substring(1);
            }
            else
            {
                sign = "0";
            }

            // Переводим число в двоичный формат
            BinaryNumber binaryNumber = BinaryNumber.TranslateToBinary(input, 30);

            // Отсеиваем 0
            if (binaryNumber.Number[0] == false && binaryNumber.Number.Count == 1)
            {
                type = NumberType.Number;
                return "0 00000000 00000000000000000000000";
            }
            
            // Мантисса
            string mantissa = "";

            // Степень
            int power = 0;

            // Убираем лишние нули в начале
            while (!binaryNumber.Number[0])
            {
                binaryNumber.Number.RemoveAt(0);
                power--;
            }

            // Добавляем к мантиссе целую часть, переведённую в двоичную систему
            for (var i = 0; i < binaryNumber.Number.Count; i++)
            {
                if (binaryNumber.Number[i])
                {
                    if (i != 0)
                    {
                        mantissa += 1;
                    }
                }
                else
                {
                    mantissa += 0;
                }
            }

            // Определяем степнь
            power += binaryNumber.Point - 1;
            
            // Переводим степень в двоичную систему
            power += 127;
            string binPower = Convert.ToString(power, 2);
            while (binPower.Length < 8)
            {
                binPower = "0" + binPower;
            }

            // Обрезаем (дополняем) мантиссу
            while (mantissa.Length < 23)
            {
                mantissa += "0";
            }
            mantissa = mantissa.Substring(0, 23);
            type = NumberType.Number;
            return sign + " " + binPower + " " + mantissa;
        }

        public static string Calculate(string expression)
        {
            string sign = "";
            expression = expression.Replace("-","+-");
            BinaryNumber summ = new BinaryNumber("0");
            string[] operands = expression.Split(new char[] { '+' });
            List<string> numbers = new List<string>();
            NumberType type;
            int maxPow = 0;
            int pow = 0;
            string mantissa = "";
            foreach (var operand in operands)
            {
                numbers.Add(Translate(operand, out type));
                pow = Convert.ToInt32(BinaryNumber.TranslateFromBinary(new BinaryNumber(numbers.Last().Substring(2, 8))));
                if (maxPow < pow)
                {
                    maxPow = pow;
                }
            }
            int resultPow = maxPow;
            foreach (var number in numbers)
            {
                pow = Convert.ToInt32(BinaryNumber.TranslateFromBinary(new BinaryNumber(number.Substring(2, 8))));
                mantissa = number.Substring(11, 23);
                mantissa = "1" + mantissa;
                while (pow < maxPow)
                {
                    mantissa = "0" + mantissa.Substring(0, mantissa.Length - 1);
                    pow++;
                }
                BinaryNumber binMantissa = new BinaryNumber(mantissa);
                if (summ.Number[0] && binMantissa.Number[0])
                {
                    resultPow++;
                }
                summ = BinaryNumber.Sum(summ, binMantissa);
                sign = "0";
                
            }
            string resultMantissa = summ.ToString().Substring(1, 23);
            return sign + " " + Convert.ToString(resultPow, 2) + " " + resultMantissa;
        }
    }

    public class BinaryNumber
    {
        public List<bool> Number { get; set; }
        public int Point { get; set; } 

        public BinaryNumber(string number)
        {
            Number = new List<bool>();
            foreach (var digit in number)
            {
                if (digit == '0')
                {
                    Number.Add(false);
                }
                else if (digit == '1')
                {
                    Number.Add(true);
                }
                else if (digit == '.')
                {
                    Point = Number.Count;
                }
                else
                {
                    throw new Exception("The number is not a binary");
                }
            }
            if (Point == 0)
            {
                Point = Number.Count;
            }
        }

        public BinaryNumber()
        {
            Number = new List<bool>();
        }

        public override string ToString()
        {
            string str = "";
            foreach (var digit in Number)
            {
                if (digit)
                {
                    str += "1";
                }
                else
                {
                    str += "0";
                }
            }
            return str;
        }

        public static BinaryNumber Sum(BinaryNumber firstSummand, BinaryNumber secondSummand)
        {
            BinaryNumber summ = new BinaryNumber();
            if (firstSummand.Number.Count < secondSummand.Number.Count)
            {
                var count = secondSummand.Number.Count - firstSummand.Number.Count;
                for (var i = 0; i < count; i++)
                {
                    firstSummand.Number.Insert(0, false);
                }
            }
            else
            {
                var count = firstSummand.Number.Count - secondSummand.Number.Count;
                for (var i = 0; i < count; i++)
                {
                    secondSummand.Number.Insert(0, false);
                }
            }
            bool carry = false;
            for (var i = Math.Max(firstSummand.Number.Count, secondSummand.Number.Count) - 1; i >= 0; i--)
            {
                if (!firstSummand.Number[i] && !secondSummand.Number[i] && !carry)
                {
                    summ.Number.Insert(0, false);
                    carry = false;
                }
                else if ((firstSummand.Number[i] && !secondSummand.Number[i] && !carry) || (!firstSummand.Number[i] && secondSummand.Number[i] && !carry) || (!firstSummand.Number[i] && !secondSummand.Number[i] && carry))
                {
                    summ.Number.Insert(0, true);
                    carry = false;
                }
                else if ((firstSummand.Number[i] && secondSummand.Number[i] && !carry) || (!firstSummand.Number[i] && secondSummand.Number[i] && carry) || (firstSummand.Number[i] && !secondSummand.Number[i] && carry))
                {
                    summ.Number.Insert(0, false);
                    carry = true;
                }
                else
                {
                    summ.Number.Insert(0, true);
                    carry = true;
                }
            }
            if (carry)
            {
                summ.Number.Insert(0, true);
            }
            return summ;
        }

        public static BinaryNumber Difference(BinaryNumber minuend, BinaryNumber subtrahend)
        {
            BinaryNumber difference = new BinaryNumber();
            if (minuend.Number.Count < subtrahend.Number.Count)
            {
                throw new Exception("Minuend is less than subtrahend");
            }
            else
            {
                var count = minuend.Number.Count - subtrahend.Number.Count;
                for (var i = 0; i < count; i++)
                {
                    subtrahend.Number.Insert(0, false);
                }
            }
            bool borrow = false;
            for (var i = minuend.Number.Count - 1; i >= 0; i--)
            {
                if ((!minuend.Number[i] && !subtrahend.Number[i] && !borrow) || (minuend.Number[i] && !subtrahend.Number[i] && borrow) || (minuend.Number[i] && subtrahend.Number[i] && !borrow))
                {
                    difference.Number.Insert(0, false);
                    borrow = false;
                }
                else if ((minuend.Number[i] && !subtrahend.Number[i] && !borrow))
                {
                    difference.Number.Insert(0, true);
                    borrow = false;
                }
                else if ((!minuend.Number[i] && subtrahend.Number[i] && borrow))
                {
                    difference.Number.Insert(0, false);
                    borrow = true;
                }
                else
                {
                    difference.Number.Insert(0, true);
                    borrow = true;
                }
            }
            if (borrow)
            {
                throw new Exception("Minuend is less than subtrahend");
            }
            return difference;
        }

        public static BinaryNumber TranslateToBinary(string number, int veracity = 10)
        {
            string[] parts = number.Split('.');
            ulong integerPart = Convert.ToUInt64(parts[0]);
            ulong fractionPart = 0;
            if (parts.Length != 1)
            {
                fractionPart = Convert.ToUInt64(parts[1]);
            }
            if (integerPart == 0 && fractionPart == 0)
            {
                return new BinaryNumber("0");
            }
            string binaryIntegerPart = "";
            ulong temp = integerPart;
            while (temp != 0)
            {
                binaryIntegerPart += temp % 2;
                temp = temp / 2;
            }
            if (binaryIntegerPart == "")
            {
                binaryIntegerPart = "0";
            }
            char[] chars = binaryIntegerPart.ToCharArray();
            Array.Reverse(chars);
            binaryIntegerPart =  new string(chars);
            string binaryFractionPart = "";
            temp = fractionPart;
            var length = 0;
            var limit = veracity;
            while (temp != 0 && limit != 0)
            {
                length = temp.ToString().Length;
                temp = temp * 2;
                if (temp.ToString().Length > length)
                {
                    temp = temp - Convert.ToUInt64(Math.Pow(10, length));
                    binaryFractionPart += 1;
                }
                else
                {
                    binaryFractionPart += 0;
                }
                limit--;
            }
            return new BinaryNumber(binaryIntegerPart + "." + binaryFractionPart);
        }

        public static string TranslateFromBinary(BinaryNumber number)
        {
            double output = 0;
            int num = 0;
            for (var digit = number.Point - 1; digit >= -(number.Number.Count - number.Point); digit--)
            {
                if (number.Number[num])
                {
                    output += Math.Pow(2, digit);
                }
                num++;
            }
            return output.ToString();
        }
    }
}
