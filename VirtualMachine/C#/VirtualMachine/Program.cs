﻿using System;
using System.Collections.Generic;
using System.IO;

namespace VirtualMachine
{
    class Program
    {
        public static List<string> memory = new List<string>();

        public static void Dump()
        {
            foreach (var e in memory)
                Console.WriteLine(e);
        }
        public static int Memory(int ip)
        {
            return int.Parse(memory[ip]);
        }
        static void Main()
        {
            
            string[] file = File.ReadAllLines(Path.Combine(Environment.CurrentDirectory, "program.txt"));
            string program = "";
            foreach (var line in file)
            {
                program += line+" ";
            }
            var prog = program.Split(' ');
            foreach (var command in prog)
            {
                memory.Add(command);
            }
            memory.RemoveAt(memory.Count-1);
            memory.Add("exit");
            Dump();
            var ip = 0;
            while (memory[ip] != "exit")
                switch (memory[ip])
                {
                    case "read":
                        Console.WriteLine("Введите число");
                        memory[Memory(ip + 1)] = Console.ReadLine();
                        ip += 2;
                        break;
                    case "readvar":
                        memory[Memory(ip + 2)] = memory[Memory(ip + 1)];
                        ip += 3;
                        break;
                    case "readconst":
                        memory[Memory(ip + 2)] = memory[ip + 1];
                        ip += 3;
                        break;
                    case "write":
                        Console.WriteLine(memory[Memory(ip + 1)]);
                        ip += 2;
                        break;
                    case "summ":
                        memory[Memory(ip + 3)] = (Memory(Memory(ip + 1)) + Memory(Memory(ip + 2))).ToString();
                        ip += 4;
                        break;
                    case "subt":
                        memory[Memory(ip + 3)] = (Memory(Memory(ip + 1)) - Memory(Memory(ip + 2))).ToString();
                        ip += 4;
                        break;
                    case "mult":
                        memory[Memory(ip + 3)] = (Memory(Memory(ip + 1)) * Memory(Memory(ip + 2))).ToString();
                        ip += 4;
                        break;
                    case "divi":
                        memory[Memory(ip + 3)] = (Memory(Memory(ip + 1)) / Memory(Memory(ip + 2))).ToString();
                        ip += 4;
                        break;
                    case "divm":
                        memory[Memory(ip + 3)] = (Memory(Memory(ip + 1)) % Memory(Memory(ip + 2))).ToString();
                        ip += 4;
                        break;
                    case "goto":
                        ip += Memory(ip + 1);
                        break;
                    case "gotoife":
                        if (memory[ip + 1] == memory[ip + 2])
                            ip = Memory(ip + 3);
                        else
                            ip += 4;
                        break;
                    case "gotoifn":
                        if (Memory(ip + 1) != Memory(ip + 2))
                            ip = Memory(ip + 3);
                        else
                            ip += 4;
                        break;
                    default:
                        Console.WriteLine("ERROR: Unidentified command. CODE: " + ip + ".");
                        ip = memory.Count;
                        break;
                }
            Console.ReadKey();
        }
    }
}
